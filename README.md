# Scheduler

[![license](https://img.shields.io/badge/license-MIT-green.svg)](./LICENSE) [![pipeline status](https://gitlab.com/ShiningTrapezohedron/scheduler/badges/master/pipeline.svg)](https://gitlab.com/ShiningTrapezohedron/scheduler/commits/master)

> This project was developed for Operating Systems and Networking by Peter Clay Holden Morris-Hind, 16062789, and can be viewed at the [Gitlab Repo](https://gitlab.com/ShiningTrapezohedron/scheduler). Please see the [./docs](./docs) directory for information pertaining to the project.

# Setup

This project uses [GNU Make](https://www.gnu.org/software/make/) to manage its lifecycle, the following targets are provided:

```bash
$ make                # Build the executable
$ make blessed        # Build the executable without ncurses
$ make checklist      # Build the checklist from LaTeX
$ make clean          # Clean all generated files
$ make devenv         # Launches a minimal Debian Strech container for testing
$ make loc            # Print LoC metrics (Requires the `loc` executable on `PATH`)
$ make run            # Build and run the executable
$ make nowarn         # Build and run the executable, ignoring warnings
$ make version        # Print the version info for the GCC being used to compile
```

You may need to install the `build-essential` or equivalent for your distribution to gain access to make and basic libraries, for example, on Debian:

```bash
$ sudo apt-get update
$ sudo apt-get install build-essential
```

By default, the `makefile` will use whatever `gcc` is on your path, but you can override this by setting `COMPILER` to the relevant version in the invocation, this is primarily useful on MacOS, which ships Clang with XCode Command Line tools, but symlinks it to `gcc`, and GCCs installed via methods such as Homebrew generally aren't on the path as `gcc` as a result. The makefile will also attempt to use `gcc-8` by default over `gcc` if present on the path.

For example, on MacOS:

```bash
# with XCode developer tools installed
$ make version
gcc -v
Apple LLVM version 10.0.0 (clang-1000.11.45.5)
Target: x86_64-apple-darwin18.5.0
Thread model: posix
InstalledDir: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin

# with GCC8 installed via Homebrew
$ make COMPILER=gcc-8 version
gcc-8 -v
Using built-in specs.
COLLECT_GCC=gcc-8
COLLECT_LTO_WRAPPER=/usr/local/Cellar/gcc/8.3.0/libexec/gcc/x86_64-apple-darwin18.2.0/8.3.0/lto-wrapper
Target: x86_64-apple-darwin18.2.0
Configured with: ../configure --build=x86_64-apple-darwin18.2.0 --prefix=/usr/local/Cellar/gcc/8.3.0 --libdir=/usr/local/Cellar/gcc/8.3.0/lib/gcc/8 --disable-nls --enable-checking=release --enable-languages=c,c++,objc,obj-c++,fortran --program-suffix=-8 --with-gmp=/usr/local/opt/gmp --with-mpfr=/usr/local/opt/mpfr --with-mpc=/usr/local/opt/libmpc --with-isl=/usr/local/opt/isl --with-system-zlib --with-pkgversion='Homebrew GCC 8.3.0' --with-bugurl=https://github.com/Homebrew/homebrew-core/issues --disable-multilib --with-native-system-header-dir=/usr/include --with-sysroot=/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk
Thread model: posix
gcc version 8.3.0 (Homebrew GCC 8.3.0)
```

# Docker

If Docker is installed, `make devenv` can be used to launch a shell inside a minimal container, containing a stripped down Debian Stretch instance, build-essential, zsh, and oh-my-zsh with a default config, in order to test the program within the intended end enviroment. This container is transient and will be deleted upon exiting (Which can be achieved simply by ending the current shell process, via `exit`).

Please note, the current directory is mounted as /scheduler in the container, which will also be the working directory upon entering the shell, so it is possible to compile a Linux Executable on another platform, which will obviously fail to run on a non linux host if exited. To bypass make's timestamp system and recompile for the host, use `make -B`.

# Style

The code in this project is strictly written in C89 (ANSI C) and is enforced with a collection of warnings raised to errors with `-Werror`. Several common idioms are in usage across the codebase as well, such as Include Guards, and the use of Preprocessor Macros in order to open up private ("static") functions for testing (Not currently implemented).

This will usually end up looking something like:

```c
/* In file.h */
#ifdef TEST
int function(void);
#endif

/* In file.c */
#ifndef TEST
static
#endif
int function(void) {
    return 0;
}
```
