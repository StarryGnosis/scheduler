# First Come First Serve

<details>
<summary>Dataset 1</summary>
<br/>

```javascript
PID, AT, BT
0,   0,  3
1,   1,  6
2,   2,  8
3,   3,  25
4,   4,  5
5,   6,  20
```

```json
Average Wait: 17
Average Turnaround: 28.167
```

## Working

```java
PID0 comes in at AT0
It does not have to wait
Burst time is 3, therefore Wait = 0, Turnaround = 3
Time is now 3
```

```javascript
PID, AT, BT
1,   1,  6
2,   2,  8
3,   3,  25
4,   4,  5
5,   6,  20
```

```java
PID1 comes in at AT1
It has to wait 2 so PID0 can finish
Burst time is 6, therefore Wait = 2, Turnaround = 8
Time is now 9
```

```javascript
PID, AT, BT
2,   2,  8
3,   3,  25
4,   4,  5
5,   6,  20
```

```java
PID2 comes in at AT2
It has to wait 7 so PID0 and 1 can finish
Burst time is 8, therefore Wait = 7, Turnaround = 15
Time is now 17
```

```javascript
PID, AT, BT
3,   3,  25
4,   4,  5
5,   6,  20
```

```java
PID3 comes in at AT3
It has to wait 14 so PID0, 1, and 2 can finish
Burst time is 25, therefore Wait = 14, Turnaround = 39
Time is now 42
```

```javascript
PID, AT, BT
4,   4,  5
5,   6,  20
```

```java
PID4 comes in at AT4
It has to wait 38 so PID0, 1, 2, and 3 can finish
Burst time is 5, therefore Wait = 38, Turnaround = 43
Time is now 47
```

```javascript
PID, AT, BT
5,   6,  20
```

```java
PID5 comes in at AT6
It has to wait 41 so PID0, 1, 2, 3, and 4 can finish
Burst time is 20, therefore Wait = 41, Turnaround = 61
```

```java
Total Wait = 0 + 2 + 7 + 14 + 38 + 41 = 102
Total Turnaround = 3 + 8 + 15 + 39 + 43 + 61 = 169

Average Wait = 102/6 = 17
Average Turnaround = 169/6 = 28.167
```
</details>

<details>
<summary>Dataset 2</summary>
<br/>

```javascript
PID, AT, BT
0,   0,  25
1,   1,  20
2,   2,  3
3,   3,  8
4,   4,  6
5,   6,  5
```

```json
Average Wait: 36.667
Average Turnaround: 47.833
```

## Working

```java
PID0 comes in at AT0
It does not have to wait
Burst time is 25, therefore Wait = 0, Turnaround = 25
Time is now 25
```

```javascript
PID, AT, BT
1,   1,  20
2,   2,  3
3,   3,  8
4,   4,  6
5,   6,  5
```

```java
PID1 comes in at AT1
It has to wait 24 so PID0 can finish
Burst time is 20, therefore Wait = 24, Turnaround = 44
Time is now 45
```

```javascript
PID, AT, BT
2,   2,  3
3,   3,  8
4,   4,  6
5,   6,  5
```

```java
PID2 comes in at AT2
It has to wait 43 so PID0 and 1 can finish
Burst time is 3, therefore Wait = 43, Turnaround = 46
Time is now 48
```

```javascript
PID, AT, BT
3,   3,  8
4,   4,  6
5,   6,  5
```

```java
PID3 comes in at AT3
It has to wait 45 so PID0, 1, and 2 can finish
Burst time is 8, therefore Wait = 45, Turnaround = 53
Time is now 56
```

```javascript
PID, AT, BT
4,   4,  6
5,   6,  5
```

```java
PID4 comes in at AT4
It has to wait 52 so PID0, 1, 2, and 3 can finish
Burst time is 6, therefore Wait = 52, Turnaround = 58
Time is now 62
```

```javascript
PID, AT, BT
5,   6,  5
```

```java
PID5 comes in at AT6
It has to wait 56 so PID0, 1, 2, 3, and 4 can finish
Burst time is 5, therefore Wait = 56, Turnaround = 61
```

```java
Total Wait = 0 + 24 + 43 + 45 + 52 + 56 = 220
Total Turnaround = 25 + 44 + 46 + 53 + 58 + 61 = 287

Average Wait = 220/6 =  36.667
Average Turnaround = 287/6 = 47.833
```
</details>

<details>
<summary>Dataset 3</summary>
<br/>

```javascript
PID, AT, BT
0,   3,  4
1,   1,  5
2,   2,  20
3,   0,  25
4,   6,  14
5,   8,  6
```

```json
Average Wait: 34.5
Average Turnaround: 46.833
```

## Working

```javascript
Rearranged by Arrival Time

PID, AT, BT
3,   0,  25
1,   1,  5
2,   2,  20
0,   3,  4
4,   6,  14
5,   8,  6
```

```java
PID3 comes in at AT0
It does not have to wait
Burst time is 25, therefore Wait = 0, Turnaround = 25
Time is now 25
```

```javascript
PID, AT, BT
1,   1,  5
2,   2,  20
0,   3,  4
4,   6,  14
5,   8,  6
```

```java
PID1 comes in at AT1
It has to wait 24 so PID3 can finish
Burst time is 5, therefore Wait = 24, Turnaround = 29
Time is now 30
```

```javascript
PID, AT, BT
2,   2,  20
0,   3,  4
4,   6,  14
5,   8,  6
```

```java
PID2 comes in at AT2
It has to wait 28 so PID3 and 1 can finish
Burst time is 20, therefore Wait = 28, Turnaround = 48
Time is now 50
```

```javascript
PID, AT, BT
0,   3,  4
4,   6,  14
5,   8,  6
```

```java
PID0 comes in at AT3
It has to wait 47 so PID3, 1, and 2 can finish
Burst time is 4, therefore Wait = 47, Turnaround = 51
Time is now 54
```

```javascript
PID, AT, BT
4,   6,  14
5,   8,  6
```

```java
PID4 comes in at AT6
It has to wait 48 so PID3, 1, 2, and 0 can finish
Burst time is 14, therefore Wait = 48, Turnaround = 62
Time is now 68
```

```javascript
PID, AT, BT
5,   8,  6
```

```java
PID5 comes in at AT8
It has to wait 60 so PID3, 1, 2, 0, and 4 can finish
Burst time is 6, therefore Wait = 60, Turnaround = 66
```

```java
Total Wait = 0 + 24 + 28 + 47 + 48 + 60 = 207
Total Turnaround = 25 + 29 + 48 + 51 + 62 + 66 = 281

Average Wait = 207/6 =  34.5
Average Turnaround = 281/6 = 46.833
```
</details>

# Round Robin

# Q2

<details>
<summary>Dataset 1</summary>
<br/>

```javascript
PID, AT, BT
0,   0,  3
1,   1,  6
2,   2,  8
3,   3,  25
4,   4,  5
5,   6,  20
```

```json
Average Wait: 26.333
Average Turnaround: 37.5
```

## Working

```java
T0:  PID0 (W0) 3  -> 1
T2:  PID1 (W1) 6  -> 4
T4:  PID2 (W2) 8  -> 6
T6:  PID3 (W3) 25 -> 23
T8:  PID4 (W4) 5  -> 3
T10: PID5 (W4) 20 -> 18
```

```java
T12: PID0 (W10) 1  -> 0 (Complete: W10, T13)
T13: PID1 (W10) 4  -> 2
T15: PID2 (W11) 6  -> 4
T17: PID3 (W12) 23 -> 21
T19: PID4 (W13) 3  -> 1
T21: PID5 (W13) 18 -> 16
```

```java
T23: PID1 (W18) 2  -> 0 (Complete: W18, T24)
T25: PID2 (W19) 4  -> 2
T27: PID3 (W20) 21 -> 19
T29: PID4 (W21) 1  -> 0 (Complete: W21, T26)
T30: PID5 (W20) 16 -> 14
```

```java
T32: PID2 (W34) 2  -> 0 (Complete: W34, T42)
T34: PID3 (W25) 19 -> 17
T36: PID5 (W24) 14 -> 12
```

```java
T38: PID3 (W27) 17 -> 15
T40: PID5 (W26) 12 -> 10
```

```java
T42: PID3 (W29) 15 -> 13
T44: PID5 (W28) 10 -> 8
```

```java
T46: PID3 (W31) 13 -> 11
T48: PID5 (W30) 8  -> 6
```

```java
T50: PID3 (W33) 11 -> 9
T52: PID5 (W32) 6  -> 4
```

```java
T54: PID3 (W35) 9 -> 7
T56: PID5 (W34) 4 -> 2
```

```java
T58: PID3 (W37) 7 -> 5
T60: PID5 (W36) 2 -> 0 (Complete: W36, T56)
```

```java
T62: PID3 (W39) 5 -> 3
T64: PID3 (W39) 3 -> 1
T65: PID3 (W39) 1 -> 0 (Complete: W39, T64)
```

```java
Total Wait = 10 + 18 + 21 + 34 + 36 + 39 = 158
Total Turnaround = 13 + 24 + 26 + 42 + 56 + 64 = 225

Average Wait = 158/6 = 26.333
Average Turnaround = 225/6 = 37.5
```
</details>

<details>
<summary>Dataset 2</summary>
<br/>

```javascript
PID, AT, BT
0,   0,  25
1,   1,  20
2,   2,  3
3,   3,  8
4,   4,  6
5,   6,  5
```

```json
Average Wait: 27.667
Average Turnaround: 38.833
```

## Working

```java
T0:  PID0 (W0) 25 -> 23
T2:  PID1 (W1) 20 -> 18
T4:  PID2 (W2) 3  -> 1
T6:  PID3 (W3) 8  -> 6
T8:  PID4 (W4) 6  -> 4
T10: PID5 (W4) 5  -> 3
```

```java
T12: PID0 (W10) 21 -> 19
T14: PID1 (W11) 18 -> 16
T16: PID2 (W12) 1  -> 0 (Complete: W12, T15)
T17: PID3 (W12) 6  -> 4
T19: PID4 (W13) 4  -> 2
T21: PID5 (W13) 3  -> 1
```

```java
T23: PID0 (W19) 19 -> 17
T25: PID1 (W20) 16 -> 14
T27: PID3 (W20) 4  -> 2
T29: PID4 (W23) 2  -> 0 (Complete: W23, T29)
T31: PID5 (W21) 1  -> 0 (Complete: W21, T26)
```

```java
T32: PID0 (W26) 17 -> 15
T34: PID1 (W27) 14 -> 12
T36: PID3 (W27) 2  -> 0 (Complete: W27, T35)
```

```java
T38: PID0 (W30) 15 -> 13
T40: PID1 (W31) 12 -> 10
```

```java
T42: PID0 (W32) 13 -> 11
T44: PID1 (W33) 10 -> 8
```

```java
T46: PID0 (W34) 11 -> 9
T48: PID1 (W35) 8  -> 6
```

```java
T50: PID0 (W36) 9 -> 7
T52: PID1 (W37) 6 -> 4
```

```java
T54: PID0 (W38) 7 -> 5
T56: PID1 (W39) 4 -> 2
```

```java
T58: PID0 (W40) 5 -> 3
T60: PID1 (W41) 2 -> 0 (Complete: W41, T61)
```

```java
T62: PID0 (W42) 3 -> 1
T63: PID0 (W42) 1 -> 0 (Complete: W42, T67)
```


```java
Total Wait = 12 + 23 + 21 + 27 + 41 + 42 = 166
Total Turnaround = 15 + 29 + 26 + 35 + 61 + 67 = 233

Average Wait = 166/6 = 27.667
Average Turnaround = 233/6 = 38.833
```
</details>

<details>
<summary>Dataset 3</summary>
<br/>

```javascript
PID, AT, BT
0,   3,  4
1,   1,  5
2,   2,  20
3,   0,  25
4,   6,  14
5,   8,  6
```

```json
Average Wait: 32.333
Average Turnaround: 44.667
```

## Working

```javascript
Rearranged by Arrival Time

PID, AT, BT
3,   0,  25
1,   1,  5
2,   2,  20
0,   3,  4
4,   6,  14
5,   8,  6
```

```java
T0:  PID3 (W0) 25 -> 23
T2:  PID1 (W1) 5  -> 3
T4:  PID2 (W2) 20 -> 18
T6:  PID0 (W3) 4  -> 2
T8:  PID4 (W2) 14 -> 12
T10: PID5 (W2) 6  -> 4
```

```java
T12: PID3 (W10) 23 -> 21
T14: PID1 (W11) 3  -> 1
T16: PID2 (W12) 18 -> 16
T18: PID0 (W13) 2  -> 0 (Complete: W13, T17)
T20: PID4 (W12) 12 -> 10
T22: PID5 (W12) 4  -> 2
```

```java
T24: PID3 (W20) 21 -> 19
T26: PID1 (W21) 1  -> 0 (Complete: W21, T26)
T27: PID2 (W21) 16 -> 14
T29: PID4 (W19) 10 -> 8
T31: PID5 (W19) 2  -> 0 (Complete: W19, T25)
```

```java
T33: PID3 (W27) 19 -> 17
T35: PID2 (W27) 14 -> 12
T37: PID4 (W25) 8  -> 6
```

```java
T39: PID3 (W31) 17 -> 15
T41: PID2 (W31) 12 -> 10
T43: PID4 (W29) 6  -> 4
```

```java
T45: PID3 (W35) 17 -> 15
T47: PID2 (W35) 12 -> 10
T49: PID4 (W33) 4  -> 2
```

```java
T51: PID3 (W39) 15 -> 13
T53: PID2 (W39) 12 -> 10
T55: PID4 (W37) 2  -> 0 (Complete: W37, T51)
```

```java
T57: PID3 (W43) 13 -> 11
T59: PID2 (W43) 10 -> 8
```

```java
T61: PID3 (W45) 11 -> 9
T63: PID2 (W45) 8  -> 6
```

```java
T65: PID3 (W47) 9 -> 7
T67: PID2 (W47) 6 -> 4
```

```java
T69: PID3 (W49) 7 -> 5
T71: PID2 (W49) 4 -> 2
```

```java
T73: PID3 (W51) 5 -> 3
T75: PID2 (W51) 2 -> 0 (Complete: W51, T71)
```

```java
T77: PID3 (W53) 3 -> 1
T79: PID3 (W53) 1 -> 0 (Complete: W53, T78)
```

```java
Total Wait = 13 + 21 + 19 + 37 + 51 + 53 = 194
Total Turnaround = 17 + 26 + 25 + 51 + 71 + 78 = 268

Average Wait = 194/6 = 32.333
Average Turnaround = 268/6 = 44.667
```
</details>


# Q4

> TODO

# Shortest Job First

<details>
<summary>Dataset 1</summary>
<br/>

```javascript
PID, AT, BT
0,   0,  3
1,   1,  6
2,   2,  8
3,   3,  25
4,   4,  5
5,   6,  20
```

```json
Average Wait: 12.333
Average Turnaround: 23.5
```

## Working

```java
PID0 arrives at T0
It does not have to wait
Burst time is 3, therefore Wait = 0, Turnaround = 3
Time is now 3
```

```javascript
PID, AT, BT
1,   1,  6
2,   2,  8
3,   3,  25
4,   4,  5
5,   6,  20
```

```java
By Time 3, PID1, 2, and 3 have arrived
PID1 is picked as it has the shortest burst time of 6
It has to wait 2 so PID0 can finish
Burst time is 6, therefore Wait = 2, Turnaround = 8
Time is now 9
```

```javascript
PID, AT, BT
2,   2,  8
3,   3,  25
4,   4,  5
5,   6,  20
```

```java
By Time 9, all remaining processes have arrived
PID4 is picked as it has the shortest burst time of 5
It has to wait 5 so PID0 and 1 can finish
Burst time is 5, therefore Wait = 5, Turnaround = 10
Time is now 14
```

```javascript
PID, AT, BT
2,   2,  8
3,   3,  25
5,   6,  20
```

```java
PID2 is picked as it has the shortest burst time of 8
It has to wait 12 so PID0, 1, and 4 can finish
Burst time is 8, therefore Wait = 12, Turnaround = 20
Time is now 22
```

```javascript
PID, AT, BT
3,   3,  25
5,   6,  20
```

```java
PID5 is picked as it has the shortest burst time of 20
It has to wait 16 so PID 0, 1, 4, and 2 can finish
Burst time is 20, therefore Wait = 16, Turnaround = 36
Time is now 42
```

```javascript
PID, AT, BT
3,   3,  25
```

```java
PID3 is picked as the last remaining process
It has to wait 39 so PID 0, 1, 4, 2, and 5 can finish
Burst time is 25, therefore Wait = 39, Turnaround = 64
```

```java
Total Wait = 0 + 2 + 5 + 12 + 16 + 39 = 74
Total Turnaround = 3 + 8 + 10 + 20 + 36 + 64 = 141

Average Wait = 74/6 = 12.334
Average Turnaround = 141/6 = 23.5
```
</details>

<details>
<summary>Dataset 2</summary>
<br/>

```javascript
PID, AT, BT
0,   0,  25
1,   1,  20
2,   2,  3
3,   3,  8
4,   4,  6
5,   6,  5
```

```json
Average Wait: 26
Average Turnaround: 37.167
```

## Working

```java
PID0 arrives at T0
It does not have to wait
Burst time is 25, therefore Wait = 0, Turnaround = 25
Time is now 25
```

```javascript
PID, AT, BT
1,   1,  20
2,   2,  3
3,   3,  8
4,   4,  6
5,   6,  5
```

```java
By Time 25, all remaining processes have arrived
PID2 is picked as it has the shortest burst time of 3
It has to wait 23 so PID0 can finish
Burst time is 3, therefore Wait = 23, Turnaround = 26
Time is now 28
```

```javascript
PID, AT, BT
1,   1,  20
3,   3,  8
4,   4,  6
5,   6,  5
```

```java
PID5 is picked as it has the shortest burst time of 5
It has to wait 22 so PID0 and 2 can finish
Burst time is 5, therefore Wait = 22, Turnaround = 27
Time is now 33
```

```javascript
PID, AT, BT
1,   1,  20
3,   3,  8
4,   4,  6
```

```java
PID4 is picked as it has the shortest burst time of 6
It has to wait 29 so PID0, 2, and 5 can finish
Burst time is 6, therefore Wait = 29, Turnaround = 35
Time is now 39
```

```javascript
PID, AT, BT
1,   1,  20
3,   3,  8
```

```java
PID3 is picked as it has the shortest burst time of 8
It has to wait 36 so PID0, 2, 5, and 4 can finish
Burst time is 8, therefore Wait = 36, Turnaround = 44
Time is now 47
```

```javascript
PID, AT, BT
1,   1,  20
```

```java
PID1 is picked as the last remaining process
It has to wait 46 so PID0, 2, 5, 4, and 3 can finish
Burst time is 20, therefore Wait = 46, Turnaround = 66
```

```java
Total Wait = 0 + 23 + 22 + 29 + 36 + 46 = 156
Total Turnaround = 25 + 26 + 27 + 35 + 44 + 66 = 223

Average Wait = 156/6 = 26
Average Turnaround = 223/6 = 37.167
```
</details>

<details>
<summary>Dataset 3</summary>
<br/>

```javascript
PID, AT, BT
0,   3,  4
1,   1,  5
2,   2,  20
3,   0,  25
4,   6,  14
5,   8,  6
```

```json
Average Wait: 27
Average Turnaround: 39.333
```

## Working

```java
PID3 arrives at T0
It does not have to wait
Burst time is 25, therefore Wait = 0, Turnaround = 25
Time is now 25
```

```javascript
PID, AT, BT
0,   3,  4
1,   1,  5
2,   2,  20
4,   6,  14
5,   8,  6
```

```java
By Time 25, all remaining processes have arrived
PID0 is picked as it has the shortest burst time of 4
It has to wait 22 so PID3 can finish
Burst time is 4, therefore Wait = 22, Turnaround = 26
Time is now 29
```

```javascript
PID, AT, BT
1,   1,  5
2,   2,  20
4,   6,  14
5,   8,  6
```

```java
PID1 is picked as it has the shortest burst time of 5
It has to wait 28 so PID3 and 0 can finish
Burst time is 5, therefore Wait = 28, Turnaround = 33
Time is now 34
```

```javascript
PID, AT, BT
2,   2,  20
4,   6,  14
5,   8,  6
```

```java
PID5 is picked as it has the shortest burst time of 6
It has to wait 26 so PID3, 0, and 1 can finish
Burst time is 6, therefore Wait = 26, Turnaround = 32
Time is now 40
```

```javascript
PID, AT, BT
2,   2,  20
4,   6,  14
```

```java
PID4 is picked as it has the shortest burst time of 14
It has to wait 34 so PID3, 0, 1, and 5 can finish
Burst time is 14, therefore Wait = 34, Turnaround = 48
Time is now 54
```

```javascript
PID, AT, BT
2,   2,  20
```

```java
PID2 is picked as the last remaining process
It has to wait 52 so PID3, 0, 1, 5, and 4 can finish
Burst time is 20, therefore Wait = 52, Turnaround = 72
```

```java
Total Wait = 0 + 22 + 28 + 26 + 34 + 52 = 162
Total Turnaround = 25 + 26 + 33 + 32 + 48 + 72 = 236

Average Wait = 162/6 = 27
Average Turnaround = 236/6 = 39.333
```
</details>

