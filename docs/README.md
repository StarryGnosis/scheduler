# Scheduler

> This project was developed for Operating Systems and Networking by Peter Clay Holden Morris-Hind, 16062789

Please see [checklist.pdf](./checklist.pdf) for information on completed sections.

# Screenshots

<details>
<summary>Menu</summary>
<br/>

### ASCII
![ASCII Menu](./screenshots/menu.png)

### Unicode
![Unicode Menu](./screenshots/menu_unicode.png)
</details>

<details>
<summary>First-Come First-Serve</summary>
<br/>

### Assignment Datasets
![Assignment Datasets](./screenshots/fcfs-datasets.png)
</details>

<details>
<summary>Round Robin</summary>
<br/>

### Assignment Datasets - Quantum 2
![Assignment Datasets](./screenshots/rr-q2-datasets.png)


### Assignment Datasets - Quantum 4
![Assignment Datasets](./screenshots/rr-q4-datasets.png)
</details>

<details>
<summary>Shortest Job First</summary>
<br/>

### Assignment Datasets
![Assignment Datasets](./screenshots/sjf-datasets.png)
</details>

<details>
<summary>Misc</summary>
<br/>

### Saving Results
![Saving Results](./screenshots/saving-results.png)

### Help
![Help](./screenshots/help-printout.png)
</details>
