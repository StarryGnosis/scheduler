/* Peter Clay Holden Morris-Hind, 16062789 */

#ifndef FCFS_H
#define FCFS_H

#include <stdio.h>
#include <stdlib.h>

#include "algorithms/algorithm.h"
#include "util/save.h"

/* Calculates Wait for each process */
int* FCFS_Wait(const Data);

/* First-Come First-Serve implementation,
   takes a Data containing PID, Arrival, and Burst Times
   and returns a table of PIDs, Waiting Time, and Turnaround Time */
void FCFS_Algorithm(Data, bool);

#endif /* FCFS_H */
