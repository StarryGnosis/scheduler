/* Peter Clay Holden Morris-Hind, 16062789 */

#ifndef RR_H
#define RR_H

#include <stdio.h>
#include <stdlib.h>

#include "algorithms/algorithm.h"
#include "util/save.h"

/* Calculates Wait for each process */
int* RR_Wait(const Data, int);

/* Round Robin implementation,
   takes a Data containing PID, Arrival, and Burst Times
   and returns a table of PIDs, Waiting Time, and Turnaround Time */
void RR_Algorithm(Data, int, bool);

#endif /* RR_H */
