/* Peter Clay Holden Morris-Hind, 16062789 */

#ifndef SJF_H
#define SJF_H

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#include "algorithms/algorithm.h"
#include "util/save.h"

/* Calculates Wait for each process */
int* SJF_Wait(const Data);

/* Shortest Job First implementation,
   takes a Data containing PID, Arrival, and Burst Times
   and returns a table of PIDs, Waiting Time, and Turnaround Time */
void SJF_Algorithm(const Data, bool);

#endif /* SJF_H */
