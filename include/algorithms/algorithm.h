/* Peter Clay Holden Morris-Hind, 16062789 */

#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "util/save.h"

/* Removes the given index from the given process array */
void processRemove(Process *, int index, int length);

/* Helper functions for the qsort() function */
int processSortByArrival (const void *, const void *);
int processSortByBurst (const void *, const void *);

/* Calculates the average Wait and Turnaround */
Averages calcAvg(const Data data, int* (*waitFunc) (Data));

/* Calculates the average Wait and Turnaround, includes quantum */
Averages calcAvgQuantum(Data data, int* (*waitFunc) (Data, const int), const int quantum);

/* Framework for all algorithms */
void algorithm(Data data, int* (*waitFunc) (Data), char *algName, bool);

/* Framework for all algorithms, includes quantum */
void algorithmQuantum(Data data, int* (*waitFunc) (Data, const int), const int quantum, char *algName, bool);

#endif /* ALGORITHM_H */
