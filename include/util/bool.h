/* Peter Clay Holden Morris-Hind, 16062789 */

#ifndef BOOL_H
#define BOOL_H

/* Define bools if not already defined, compatible with Curses' bools*/
#ifndef TRUE
typedef int bool;
#define TRUE 1
#define FALSE 0
#endif

#endif /* BOOL_H */
