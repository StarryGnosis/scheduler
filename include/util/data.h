/* Peter Clay Holden Morris-Hind, 16062789 */

#ifndef DATA_H
#define DATA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util/bool.h"
#include "util/macro.h"

typedef enum algorithmEnum {
    FCFS, RR, SJF, None
} algorithmEnum;

struct Args_t {
    algorithmEnum alg;
    int quantum;
    char **inputFiles;
    int numInputFiles;
    bool save;
} Args;

/* Holds the raw data from the datafile */
typedef struct DataFile {
    char **data;
    int fileLines;
} DataFile;

/* Holds a single Process' information */
typedef struct Process {
    int procid;
    int arrival;
    int burst;
} Process;

/* Holds the parsed data from the datafile */
typedef struct Data {
    Process *process;
    const char *filename;
    int numItems;
} Data;

char *p_strdup(const char *);
void displayUsage(void);

#ifdef TEST
/* Returns fileLines = -1 to indicate non existence,
    or a char buffer containing the file contents
    if the file exists */
DataFile fileExists(const char *);

/* Attempts to parse MaybeFile Data into the Data
   Structure, returns { NULL, NULL, -1 } if parsing
   is not possible */
Data parseData(char **, const char *, int);
#endif

/* Attempts to parse the given file into the Data struct,
    returns { NULL, NULL, -1 } if parsing wasn't possible */
Data datafile(const char *);

#endif /* DATA_H */
