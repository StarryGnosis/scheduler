/* Peter Clay Holden Morris-Hind, 16062789 */

#ifndef MACRO_H
#define MACRO_H

/* Array Length */
#define LENGTH(a) (sizeof(a) / sizeof(a[0]))

/* TERM CONSOLE COLOURS */
#define COL_RES    "\x1b[0m"
#define COL_R      "\x1b[31m"
#define RED(s)     COL_R""s""COL_RES
#define COL_G      "\x1b[32m"
#define GREEN(s)   COL_G""s""COL_RES
#define COL_Y      "\x1b[33m"
#define YELLOW(s)  COL_Y""s""COL_RES
#define COL_B      "\x1b[34m"
#define BLUE(s)    COL_B""s""COL_RES
#define COL_M      "\x1b[35m"
#define MAGENTA(s) COL_M""s""COL_RES
#define COL_C      "\x1b[36m"
#define CYAN(s)    COL_C""s""COL_RES

#endif /* MACRO_H */
