/* Peter Clay Holden Morris-Hind, 16062789 */

#ifndef MENU_H
#define MENU_H

#include <stdlib.h>
#include <string.h>
#include <menu.h>

#include "algorithms/FCFS.h"
#include "algorithms/RR.h"
#include "algorithms/SJF.h"
#include "util/data.h"

typedef struct Func {
    void (*fn)(const char *);
} Func;

void render(const char **, size_t);
Func menuFunction(void (*)(const char *));

#endif /* MENU_H */
