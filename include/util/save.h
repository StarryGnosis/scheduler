/* Peter Clay Holden Morris-Hind, 16062789 */

#ifndef SAVE_H
#define SAVE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* POSIX */
#include <libgen.h>
#include <sys/stat.h>

#include "util/data.h"

/* Holds the averages calculation */
typedef struct Averages {
    float wait;
    float turnaround;
} Averages;

/* Holds data for writing out results */
typedef struct SaveData {
    const Data *data;
    const Averages *averages;
} SaveData;

#ifdef TEST
/* This will attempt to remove the .extension from a given filepath string

   BASED ON: https://stackoverflow.com/a/43163740 */
void stripExtension(char *);
#endif

/* Saves the given data in ./save, with a filename
   equal to the following, dash seperated string:

   <Original Filename without Extension>
   <UTC Time Hex>
   <Short Algorithm Name>
   Q<Quantum> (If present)
   .scdr

   For example: dataset1-5ca09ab7-RR-Q1.scdr */
int save (SaveData, char *);

#endif /* SAVE_H */
