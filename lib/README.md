# Library Directory

This directory contains support libraries that are usually installed with the operating system's package manager but cannot for the purposes of the assessment. These currently include: libncurses5. This is stripped down to what is necessary to statically link the library into the final executable, as tested on a Debian Stretch Image with build-essential only.

No code or libraries within this directory were written by me (Peter Clay Holden Morris-Hind)
