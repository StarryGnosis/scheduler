# Peter Clay Holden Morris-Hind, 16062789

# Recursive Wildcard, used to easily pick up files under subfolders
# Added sort and strip to make the command line printout nicer
# https://stackoverflow.com/a/18258352
rwildcard=$(sort $(strip $(foreach d, $(wildcard $1*), $(call rwildcard, $d/, $2) $(filter $(subst *, %, $2), $d))))

# Var used to do stuff relative to the Makefile
MAKEFILE_DIR := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))

.NOTPARALLEL:
SHELL = /bin/sh
WARNINGS=-Wall -pedantic -pedantic-errors -Werror -Wextra -Wformat

# See https://access.redhat.com/blogs/766093/posts/1976213 for _FORTIFY_SOURCE
FLAGS=${WARNINGS} -std=c89 -O3 -D_FORTIFY_SOURCE=2

# Extremely horrible way of getting ncurses working on Debian when
# libncurses5-dev is not installed
UNAME_S := $(shell uname -s)
UNAME_M := $(shell uname -m)

ifeq ($(UNAME_S), Darwin)
    # We're on MacOS and can just link against the system version
    LINKERFLAGS = -lmenu -lncurses
else
    # Link against the shipped versions in ./lib
    ifeq ($(UNAME_M), x86_64)
        LINKERFLAGS = -L${MAKEFILE_DIR}/lib/curses/x64
    else
        LINKERFLAGS = -L${MAKEFILE_DIR}/lib/curses/x86
    endif

    LINKERFLAGS += -I./lib/curses/include
    LINKERFLAGS += -Wl,--start-group -Wl,-Bstatic
    # libgpm seems to be mistakenly fixed as a hard dep in 6.1+20180210-1 onwards
    # But reconfiguring the library from source is too time consuming
    LINKERFLAGS += -lmenu -lncurses -ltinfo -lgpm
    LINKERFLAGS += -Wl,--end-group -Wl,-Bdynamic
endif

COMPILER := $(or $(COMPILER), "gcc")

EXE=scheduler

SRCDIR=./src
INCDIR=./include

SRC=$(call rwildcard, ${SRCDIR}, *.c)
INCSRC=$(call rwildcard, ${INCDIR}, *.h)

LATEXSOURCE=checklist
LATEXFILES=$(filter-out ./docs/${LATEXSOURCE}.tex, $(filter-out ./docs/${LATEXSOURCE}.pdf, $(wildcard ./docs/${LATEXSOURCE}.*)))

.DEFAULT_GOAL := ${EXE}

${EXE}: ${SRC} ${INCSRC} makefile
	${COMPILER} ${FLAGS} ${SRC} -I${INCDIR} -DCURSED -o ${EXE} ${LINKERFLAGS}

.PHONY: blessed
blessed: ${SRC} ${INCSRC} makefile
	${COMPILER} ${FLAGS} $(filter-out ./src/util/menu.c, ${SRC}) -I${INCDIR} -o ${EXE}

.PHONY: run
run: ${EXE}
	./${EXE}

.PHONY: nowarn
nowarn: ${SRC} ${INCSRC} main.c makefile
	${COMPILER} ${FLAGS} ${SRC} -I${INCDIR} -o ${EXE} -w ${LINKERFLAGS}
	./scheduler

.SILENT: checklist
checklist: ./docs/checklist.tex
	cd ./docs && command -v latexmk > /dev/null 2>&1 && latexmk -lualatex -g checklist || echo "LaTeX Make (latexmk) not found, doing nothing"

.PHONY: loc
loc:
	command -v loc > /dev/null 2>&1 && loc ./src ./include || echo "loc not found, doing nothing"

.PHONY: clean
clean:
	rm -rf ${EXE} ${LATEXFILES} $(wildcard *.dSYM) ./save/

.PHONY: version
version:
	${COMPILER} -v


# Not sure why this works but
# this is needed for subst below to work correctly
define newline


endef

# This allows for testing on Debian Stretch via Docker
define DOCKER_SETUP
apt-get update
apt-get install build-essential git wget zsh -y
wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O - | zsh || true
zsh
endef

.PHONY: devenv
devenv:
	docker run -it -v ${MAKEFILE_DIR}:/scheduler -w /scheduler --rm debian:stretch-slim /bin/bash -c "$(subst $(newline), && ,${DOCKER_SETUP})"
