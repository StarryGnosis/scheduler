/* Peter Clay Holden Morris-Hind, 16062789 */

/***  FIRST COME FIRST SERVE ***/

/*  DESCRIPTION
    The First Come, First Serve algorithm works by simply queuing up processes in the order
    they arrive and doesn't preempt, meaning that each process is executed until it finishes
    once started.
*/

/*  PROS
    FCFS is one of the simplest to implement and understand, and therefore has the advantage of
    being easy to immediately grasp. It can also be used as a good example for demonstrating the
    general principles involved in scheduling. Every task will get a chance to run (Assuming all
    tasks terminate) so this algorithm does not cause starvation.
*/

/*  CONS
    Because this algorithm is non preemptive, tasks later in the queue are blocked until earlier to
    arrive ones finish, this means that tasks with a short burst time can have quite a high
    turnaround time if they end up queued behind long running tasks. This can lead to
    inefficiences in IO as many tasks may end up waiting for a single task to complete slow
    IO actions.
*/

#include "algorithms/FCFS.h"

int* FCFS_Wait(Data data) {
    Process *process = malloc((size_t) data.numItems * sizeof(Process));
    int *sum = malloc((size_t) data.numItems * sizeof(int));
    int *wait = malloc((size_t) data.numItems * sizeof(int));
    int i;

    /* Create copy of processes to operate on */
    memcpy(process, data.process, sizeof(Process) * (size_t) data.numItems);

    /* Sort by arrival time */
    qsort(process, (size_t) data.numItems, sizeof(Process), processSortByArrival);

    /* The first process will not have to wait */
    sum[0] = 0;
    wait[0] = 0;

    /* Iterate from the second item until the end of the queue
       (already sorted by arrival time) */
    for (i = 1; i < data.numItems; ++i) {
        /* Accumulate a running total of the previous
           process' burst times */
        sum[i] = sum[i-1] + process[i-1].burst;

        /* Subtract the arrival time from the raw wait
           calculated above */
        wait[i] = sum[i] - process[i].arrival;

        /* If waiting time is negative,
           the process didn't have to wait */
        if (wait[i] < 0)
            wait[i] = 0;
    }

    /* Memory cleanup */
    free(sum);
    free(process);

    return wait;
}

void FCFS_Algorithm(const Data data, bool save) {
    char algName[5] = "FCFS";
    algorithm(data, FCFS_Wait, algName, save);
}
