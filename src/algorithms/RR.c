/* Peter Clay Holden Morris-Hind, 16062789 */

/***  ROUND ROBIN ***/

/*  DESCRIPTION
    The Round Robin algorithm works by splitting time into equal sized chunks, known as quanta,
    and assigning these to processes in turn. If a process does not finish within the specified
    quanta, the process is suspended and the next process is started.

    This algorithm can be implemented in several ways, if a process finishes before the quanta
    expires, the scheduler can choose to immediately run the next process, or the next process can
    wait for the end of the fixed timeslice. Although the former is much more common in actual
    Operating Systems, the latter is somewhat simpler to implement, and therefore is presented
    here.
*/

/*  PROS
    Round Robin is easy to implement and is a commonly used algorithm in many areas of computing,
    including Networking, so familiarity within the field is high. It is starvation free, and
    operates "fairly" in that all processes will get a chance to execute. The worst-case wait for
    a given process to start can also be trivially determined if the total number of processes is
    known ahead of time by simply multiplying the quantum by the number of processes minus one.
*/

/*  CONS
    Some usecases are not ideal for equal timesharing, for example highly interactive processes
    may appear sluggish to end users if constantly interupted for CPU Bound tasks. Performance is
    also heavily dependent on selecting the correct quantum, if it is too large, it may as well be
    FCFS, if it is too low, the throughput will suffer as time will be dominated by process
    switching.
*/

#include "algorithms/RR.h"

int* RR_Wait(Data data, const int quantum) {
    Process *process = malloc((size_t) data.numItems * sizeof(Process));
    int *wait = malloc(sizeof(int) * (size_t) data.numItems);
    int i, j, k, currBurst, time = 0, numProcessed = 0;
    bool run = TRUE;

    /* Init all waiting times to 0 */
    for(i = 0; i < data.numItems; ++i)
        wait[i] = 0;

    /* Create copy of processes to operate on */
    memcpy(process, data.process, sizeof(Process) * (size_t) data.numItems);

    /* Sort by arrival time */
    qsort(process, (size_t) data.numItems, sizeof(Process), processSortByArrival);

    /* Run this loop until we tell it to stop */
    while (run) {
        /* Loop though the processes array */
        for(i = 0; i < data.numItems - numProcessed; ++i) {
            /* Skip over processes that have not yet arrived */
            if(process[i].arrival > time)
                continue;

            /* Test the remaining burst time of the current process*/
            currBurst = process[i].burst;

            /* Allocate the slice and advance time */
            if(currBurst - quantum > 0) {
                /* Decrease the process' burst time by the quantum
                   and increase time by the quantum */
                process[i].burst -= quantum;
                time += quantum;

                /* Increment the waiting time of the remaining processes by the quantum */

                /* For all processes originally input */
                for(j = 0; j < data.numItems; ++j) {
                    /* if the process id doesn't equal the one being dealt with */
                    if(data.process[j].procid != process[i].procid) {
                        /* For all processes in the set of processes under consideration */
                        for(k = 0; k < data.numItems - numProcessed; ++k) {
                            /* Find the index to match it up to the wait array */
                            if (process[k].procid == data.process[j].procid) {
                                /* And add the quantum */
                                wait[j] += quantum;
                            }
                        }
                    }
                }
            } else {
                /* The process has finished and needs removing from the array
                   time only advanced by the amount of time the process had left */
                processRemove(process, i, data.numItems - numProcessed);
                time += currBurst;
                ++numProcessed;

                /* Increment the waiting time of the remaining processes after this one
                   by the burst time of this process */

                /* For all processes originally input */
                for(j = 0; j < data.numItems; ++j) {
                     /* if the process id doesn't equal the one being dealt with */
                    if(data.process[j].procid != process[i].procid) {
                        /* For all processes later in thw queue in the set of processes under consideration */
                        for(k = i + 1; k < data.numItems - numProcessed; ++k) {
                            /* Find the index to match it up to the wait array */
                            if (process[k].procid == data.process[j].procid) {
                                 /* And add the burst time of the finished process */
                                wait[j] += currBurst;
                            }
                        }
                    }
                }
            }
        }

        /* Break the loop once we've processed everything */
        if(numProcessed == data.numItems)
            run = FALSE;
    }

    return wait;
}

void RR_Algorithm(const Data data, const int quantum, bool save) {
    char algName[3] = "RR";
    algorithmQuantum(data, RR_Wait, quantum, algName, save);
}
