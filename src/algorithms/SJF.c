/* Peter Clay Holden Morris-Hind, 16062789 */

/***  SHORTEST JOB FIRST ***/

/*  DESCRIPTION
    The Shortest Job First simply selects the process with the shortest burst time to execute
    next and thus is very simple. It sometimes is given the name SJN (Shortest Job Next) or
    SPN (Shortest Process Next)
*/

/*  PROS
    High Throughput when many short tasks are needed to execute. Can be highly efficient in
    enviroments where predictable patterns can be used to estimate process burst time, and as
    a technique for maintaining the responsiveness of a interactive process, which usually follow
    a pattern of burst - wait for command - burst - wait for command...
*/

/*  CONS
    Requires knowing, or at least estimating the execution time ahead of time, which is often
    difficult to impossible with real world programs. It can also result in starvation if shorter
    tasks are continually scheduled ahead of  a long task that gets stuck waiting forever.
*/

#include "algorithms/SJF.h"

int* SJF_Wait(Data data) {
    Process *process = malloc((size_t) data.numItems * sizeof(Process));
    int *wait = malloc((size_t) data.numItems * sizeof(int));
    Process *current = NULL, *tmp = NULL;
    int c = 0, time = 0, numProcessed = 1;
    int i, j, lowestBurst, lowestBurstIndex, prevArr;

    /* Copy the process array */
    memcpy(process, data.process, sizeof(Process) * (size_t) data.numItems);

    /* Sort by arrival time */
    qsort(process, (size_t) data.numItems, sizeof(Process), processSortByArrival);

    /* The first process will not have to wait */
    wait[0] = 0;

    /* Time has incremented by the burst time of the first
       process with  the lowest burst time */

    /* Work out which process this is */
    prevArr = process[0].arrival;
    lowestBurst = process[0].burst;
    lowestBurstIndex = 0;

    for(i = 0; i < data.numItems; ++i) {
        if(process[i].arrival > prevArr)
            break;

        if(process[i].burst < lowestBurst) {
            lowestBurst = process[i].burst;
            lowestBurstIndex = i;
        }
    }

    /* Actually increment time by this much */
    time += process[lowestBurstIndex].burst;

    /* Remove the just processed process */
    processRemove(process, lowestBurstIndex, data.numItems);

    /* We start from 1 in this loop to account for the
       previously removed process */
    for(i = 1; i < data.numItems; ++i) {
        /* First, create array of processes that have arrived
           by the current time */

        /* Reset current array */
        free(current);

        current = NULL;
        c = 0;

        /* Add all processes that have arrived to the current array */
        for(j = 0; j < data.numItems - numProcessed; ++j) {
            if(process[j].arrival <= time) {
                ++c;
                tmp = realloc(current, c * sizeof(Process));

                /* Handle OOM */
                if (tmp == NULL) {
                    exit(EXIT_FAILURE);
                }

                /* Reassign Pointer */
                current = tmp;
                current[c - 1] = process[j];
            }
        }

        /* Sort by burst time */
        qsort(current, (size_t) c, sizeof(Process), processSortByBurst);

        /* The process that we need will always be the
           first in the array */
        wait[i] = time - current[0].arrival;
        time += current[0].burst;

        /* Find this process in the processes array and remove it */
        for(j = 0; j < data.numItems - numProcessed; ++j) {
            if(process[j].procid == current[0].procid) {
                processRemove(process, j, data.numItems - numProcessed);
                ++numProcessed;
                break;
            }
        }
    }

    free(current);

    return wait;
}

void SJF_Algorithm(const Data data, bool save) {
    char algName[4] = "SJF";
    algorithm(data, SJF_Wait, algName, save);
}
