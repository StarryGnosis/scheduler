/* Peter Clay Holden Morris-Hind, 16062789 */

#include <stdlib.h>
#include <stdio.h>

#include "algorithms/algorithm.h"

void processRemove(Process *array, int index, int length) {
    Process *tmp;
    int i;

    /* Shift the following elements down one */
    for(i = index; i < length - 1; i++)
        array[i] = array[i + 1];

    /* Contract the size of the array */
    tmp = realloc(array, (length - 1) * sizeof(Process));

    /* Handle OOM */
    if (tmp == NULL && length > 1) {
        exit(EXIT_FAILURE);
    }

    /* Or reassign the pointer */
    array = tmp;
}

int processSortByArrival (const void *a, const void *b) {
    return (*(const Process*) a).arrival - (*(const Process*) b).arrival;
}

int processSortByBurst (const void *a, const void *b) {
    return (*(const Process*) a).burst - (*(const Process*) b).burst;
}

static int* calcTurnaround(Data data, int *wait) {
    int* turnaround = malloc((size_t) data.numItems * sizeof(int));
    int i;

    /* Turnaround is Burst + Wait */
    for (i = 0; i < data.numItems; ++i)
        turnaround[i] = data.process[i].burst + wait[i];

    return turnaround;
}

static Averages calcAvgCommon(Data data, int *wait, int *turnaround) {
    int total_wait = 0, total_turnaround = 0;
    Averages ret;
    int i;

    /* Accumulate Turnaround and Wait */
    for (i = 0; i < data.numItems; ++i) {
        total_wait = total_wait + wait[i];
        total_turnaround = total_turnaround + turnaround[i];
    }

    /* Calculate Averages */
    ret.wait = (float) total_wait / (float) data.numItems;
    ret.turnaround = (float) total_turnaround / (float) data.numItems;

    free(wait);
    free(turnaround);

    return ret;
}

Averages calcAvg(Data data, int* (*waitFunc) (Data)) {
    int *wait = malloc((size_t) data.numItems * sizeof(int));
    int *turnaround = malloc((size_t) data.numItems * sizeof(int));

    /* If there is only one process, it'll never have to wait */
    if(data.numItems == 1) {
        wait[0] = 0;
    } else {
        wait = waitFunc(data);
    }

    turnaround = calcTurnaround(data, wait);

    return calcAvgCommon(data, wait, turnaround);
}

Averages calcAvgQuantum(Data data, int* (*waitFunc) (Data, const int), const int quantum) {
    int *wait = malloc((size_t) data.numItems * sizeof(int));
    int *turnaround = malloc((size_t) data.numItems * sizeof(int));

    /* If there is only one process, it'll never have to wait */
    if(data.numItems == 1) {
        wait[0] = 0;
    } else {
        wait = waitFunc(data, quantum);
    }

    turnaround = calcTurnaround(data, wait);

    return calcAvgCommon(data, wait, turnaround);
}

static void algorithmCommon(Data data, Averages av, char *algName, bool saveFile) {
    SaveData ret;
    int i;

    ret.data = malloc(sizeof(data));
    ret.data = &data;
    ret.averages = &av;

    if(data.numItems != -1) {
        printf("\n┌── %s : %s\n", data.filename, algName);

        printf("│ P  A  B\n");
        printf("├────────\n");

        for(i = 0; i < data.numItems; ++i) {
            printf("│ %i, %i, %i\n", data.process[i].procid, data.process[i].arrival, data.process[i].burst);
        }

        printf("│\n");
        printf("├─ Av. Wait: %.3f\n├─ Av. Turn: %.3f\n", (double) ret.averages -> wait, (double) ret.averages -> turnaround);
        printf("└────────────────────────────────\n\n");
    }

    if(saveFile == TRUE)
        save(ret, algName);
}

void algorithm(Data data, int* (*waitFunc) (Data), char *algName, bool save) {
    Averages av = calcAvg(data, waitFunc);
    algorithmCommon(data, av, algName, save);
}

void algorithmQuantum(Data data, int* (*waitFunc) (Data, const int), const int quantum, char *algName, bool save) {
    Averages av = calcAvgQuantum(data, waitFunc, quantum);
    char *algNameQuantum = malloc(sizeof(algName) + (sizeof(char) * 6));

    if(quantum > 999 || quantum < 1) {
        fprintf(stderr, "scheduler time quantum must be >0 and <1000\n");
        exit(1);
    }

    sprintf(algNameQuantum, "%s-Q%d", algName, quantum);
    algorithmCommon(data, av, algNameQuantum, save);
}
