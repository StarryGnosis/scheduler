/* Peter Clay Holden Morris-Hind, 16062789 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "algorithms/FCFS.h"
#include "algorithms/RR.h"
#include "algorithms/SJF.h"
#include "util/data.h"

#ifdef CURSED
#include "util/menu.h"
#endif

static struct option Options[] = {
    {"fcfs",    no_argument,       0, 'f'},
    {"FCFS",    no_argument,       0, 'f'},
    {"rr",      required_argument, 0, 'r'},
    {"RR",      required_argument, 0, 'r'},
    {"sjf",     no_argument,       0, 's'},
    {"SJF",     no_argument,       0, 's'},
    {"help",    no_argument,       0, 'h'},
    {"save",     no_argument,      0, 'x'},
    {0, 0, 0, 0}
};

const char OPTS[] = "fFhsSr:R:xX";
int c, iOption = 0;

const char *choices[] = {
    "FCFS (First Come, First Serve)",
    "RR (Round Robin)",
    "SJF (Shortest Job First)",
    "Exit"
};

/* Returns a human readable name for the Algorithm Enum */
static const char* getAlgorithmName(enum algorithmEnum a) {
   switch (a) {
        case FCFS: return "First Come First Serve";
        case RR:   return "Round Robin";
        case SJF:  return "Shortest Journey First";
        case None:
        default:   return "UNKNOWN";
   }
}

static void runAlgorithm(enum algorithmEnum a, Data *inputFiles, int numInputFiles, int quantum, bool save) {
    int i;

    for (i = 0; i < numInputFiles; ++i) {
        switch (a) {
            case FCFS:
                FCFS_Algorithm(inputFiles[i], save);
                break;
            case RR:
                RR_Algorithm(inputFiles[i], quantum, save);
                break;
            case SJF:
                SJF_Algorithm(inputFiles[i], save);
                break;
            case None:
            default:
                fprintf(stderr, "scheduler: unrecognised algorithm");
                displayUsage();
        }
    }
}

static void determineAlg(enum algorithmEnum check) {
    const char *errorformat = "scheduler: cannot use %s with %s\n";

    if(Args.alg == None) {
        Args.alg = check;
    } else if(Args.alg == check) {
        return;
    } else {
        fprintf(stderr, errorformat, getAlgorithmName(check), getAlgorithmName(Args.alg));
        displayUsage();
    }
}

int main(int argc, char **argv) {
    Data *datafiles = NULL;
    int i, j;

    Args.alg = None;
    Args.quantum = 0;
    Args.inputFiles = NULL;
    Args.numInputFiles = 0;
    Args.save = FALSE;

    while (1) {
        c = getopt_long (argc, argv, OPTS, Options, &iOption);
        if (c == -1) break;

        switch (c) {
            case 'x':
            case 'X':
                Args.save = TRUE;
                break;

            case 'f':
            case 'F':
                determineAlg(FCFS);
                break;

            case 'r':
            case 'R':
                determineAlg(RR);
                Args.quantum = atoi(optarg);

                if (Args.quantum < 1 || Args.quantum > 999) {
                    fprintf(stderr, "scheduler: Round Robin Time Quantum must be >0 and <1000\n");
                    displayUsage();
                }
                break;

            case 's':
            case 'S':
                determineAlg(SJF);
                break;

            case 'h':
            case '?':
                displayUsage();
                break;

            default:
                break;
        }
    }

    Args.inputFiles = argv + optind;
    Args.numInputFiles = argc - optind;

    #ifdef CURSED
    if (Args.alg == None && Args.numInputFiles == 0) {
        render(&choices[0], LENGTH(choices));
    } else
    #endif
    if (Args.alg == None) {
        fprintf(stderr, "scheduler: Algorithm must be specified\n");
        displayUsage();
    } else if(Args.numInputFiles > 0) {
        datafiles = malloc(sizeof(Data) * (size_t) Args.numInputFiles);

        for(i = 0; i < Args.numInputFiles; ++i) {
            datafiles[i] = datafile(Args.inputFiles[i]);
        }

        runAlgorithm(Args.alg, datafiles, Args.numInputFiles, Args.quantum, Args.save);

        for(j = 0; j < Args.numInputFiles; ++j) {
            free(datafiles[j].process);
        }
    } else {
        datafiles = malloc(sizeof(Data) * 3);
        datafiles[0] = datafile("./data/dataset1.csv");
        datafiles[1] = datafile("./data/dataset2.csv");
        datafiles[2] = datafile("./data/dataset3.csv");

        runAlgorithm(Args.alg, datafiles, 3, Args.quantum, Args.save);

        for(j = 0; j < 3; ++j) {
            free(datafiles[j].process);
        }
    }

    free(datafiles);
    return(0);
}
