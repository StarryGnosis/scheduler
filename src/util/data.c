/* Peter Clay Holden Morris-Hind, 16062789 */

#include "util/data.h"

static size_t MAX_LINE_LENGTH = 30;

/* Implementation of the Dynamic TR Function strdup */
char *p_strdup(const char *source) {
    char *nstring;
    char *pointer;
    int length = 0;

    while (source[length])
        ++length;

    nstring = malloc(length + 1);
    pointer = nstring;

    while (*source)
        *pointer++ = *source++;

    *pointer = '\0';
    return nstring;
}

static size_t countLines(FILE *file) {
    int last = '\n', c;
    size_t lines = 0;

    while (EOF != (c = fgetc(file))) {
        if (c == '\n' && last != '\n') {
            ++lines;
        }

        last = c;
    }

    rewind(file);
    return lines;
}

/* Displays the Help Message */
void displayUsage(void) {
    printf("\n%s - An Implementation of several basic Scheduling Algorithms in ANSI C\n", GREEN("scheduler"));
    puts("\nUsage:");
    printf("%s       Use interactively\n\n", GREEN("scheduler"));
    printf("%s [%s|%s] [%s|%s %s] [%s|%s] [%s] [%s]",
        GREEN("scheduler"), YELLOW("-f"), YELLOW("--fcfs"), YELLOW("-r"), YELLOW("--r"),
        MAGENTA("<quantum>"), YELLOW("-s"), YELLOW("--sjf"), YELLOW("-x"), GREEN("datafiles..."));
    puts("");
    printf("%s              Use the First Come First Serve Algorithm\n", YELLOW("-f"));
    printf("%s %s    Use the Round Robin Algorithm with the specified time quantum\n",
        YELLOW("-r"), MAGENTA("<quantum>"));
    printf("%s              Use the Shortest Job First Algorithm\n", YELLOW("-s"));
    printf("%s              Save the output to ./save/\n", YELLOW("-x"));
    exit(EXIT_FAILURE);
}

#ifndef TEST
static
#endif
DataFile fileExists(const char *filename) {
    DataFile ret;
    FILE *file;

    if ((file = fopen(filename, "r"))) {
        char *line = malloc(sizeof(char) * MAX_LINE_LENGTH), *databuf = NULL;
        int fileLine = 0;
        char *fGetRes;

        ret.data = malloc(countLines(file) * (MAX_LINE_LENGTH * sizeof(char)));
        ret.fileLines = 0;

        while ((fGetRes = fgets(line, MAX_LINE_LENGTH, file)) != NULL) {
            ++fileLine;

            databuf = malloc((size_t) MAX_LINE_LENGTH * sizeof(char));

             if (databuf) {
                ret.data[ret.fileLines] = databuf;
            } else {
                fprintf(stderr, "ERROR: Could not allocate memory for reading %s\n", filename);
                exit(1);
            }

            ret.data[ret.fileLines] = p_strdup(line);
            ++ret.fileLines;
        }

        free(databuf);
        fclose (file);
    } else {
        ret.fileLines = -1;
    }

    return ret;
}

#ifndef TEST
static
#endif
Data parseData(char **data, const char *fileName, int entries) {
    Data ret = { NULL, NULL, 0 };
    char **lines = malloc(entries * sizeof *data);
    int *procid = (int*) malloc((size_t) entries * sizeof(int));
    int *arrival = (int*) malloc((size_t) entries * sizeof(int));
    int *burst = (int*) malloc((size_t) entries * sizeof(int));
    int n[3] = {0};
    int parsed = 0;
    int i;

    ret.filename = fileName;

    /* For reasons beyond my debugging comprehension, sometimes the
       final line is gone by the second iteration of the parser loop,
       so I copy all the data into another variable here. This is the
       source of 99% of the file parsing bugs in this program, so if
       something is going wrong, it's likely related to this. */
    for (i = 0; i < entries; ++i) {
        lines[i] = p_strdup(data[i]);
    }

    for(i = 0; i < entries; ++i) {
        /* Will prevent "Could not parse" message
           because of extra whitespace in the file */
        if(strcmp(lines[i], "\n") == 0)
            continue;

        /* http://man7.org/linux/man-pages/man3/fscanf.3p.html
           This format string will parse space/tab/comma seperated numbers
           with some tolerance for excess data (For example, extra characters
           at the end of a line are skipped entirely) */
        if (3 > sscanf(lines[i], "%d%*[ ,\t]%d%*[ ,\t]%d", &n[0], &n[1], &n[2])) {
            printf("ERROR: Could not parse L%i, proceeding\n", i + 1);
            continue;
        }

        procid[parsed] = n[0];
        arrival[parsed] = n[1];
        burst[parsed] = n[2];
        ++parsed;
    }

    for(i = 0; i < entries; ++i)
        free(lines[i]);

    free(lines);

    ret.process = malloc(sizeof(Process) * (size_t) parsed);

    if (parsed > 0) {
        for(i = 0; i < (int) parsed; ++i) {
            ret.process[i].procid = procid[i];
            ret.process[i].arrival = arrival[i];
            ret.process[i].burst = burst[i];
        }

        ret.numItems = parsed;
    } else {
        ret.numItems = -1;
    }

    return ret;
}

Data datafile(const char *filename) {
    DataFile file = fileExists(filename);
    Data data = { NULL, NULL, 0 };

    if (file.fileLines != -1) {
        data = parseData(file.data, filename, file.fileLines);
        free(file.data);
    }

    return data;
}
