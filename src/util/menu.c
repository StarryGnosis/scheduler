/* Peter Clay Holden Morris-Hind, 16062789 */

#include "util/menu.h"

/* Global within this compilation unit, so the function in the menu selection
   handler can reference them to clean up memory, probably a better way of doing
   this but I spent too long on the menu in the first place */
static WINDOW *appWindow;
static MENU *appMenu;
static ITEM **menuItems;
static size_t numChoices;

/* Set up the terminal for NCurses Rendering */
static void init(void) {
    initscr();
    start_color();
    cbreak();
    noecho();
    intrflush(stdscr, FALSE);
    curs_set(0);

    init_pair(1, COLOR_GREEN, COLOR_BLACK);
}

/* Clean up allocated memory and shut down NCurses */
static void teardown(void) {
    int i;

    unpost_menu(appMenu);
    free_menu(appMenu);

    delwin(appWindow);

    for(i = 0; i < (int) numChoices; i++) {
        free_item(menuItems[i]);
    }

    free(menuItems);

    noraw();
    endwin();
}

/* Run the actual algorithm */
static void runAlgorithm(enum algorithmEnum a, Data *inputFiles, int numInputFiles, int quantum) {
    int i;

    for (i = 0; i < numInputFiles; ++i) {
        switch (a) {
            case FCFS:
                FCFS_Algorithm(inputFiles[i], FALSE);
                break;
            case RR:
                RR_Algorithm(inputFiles[i], quantum, FALSE);
                break;
            case SJF:
                SJF_Algorithm(inputFiles[i], FALSE);
                break;
            case None:
            default:
                fprintf(stderr, "scheduler: unrecognised algorithm");
                displayUsage();
        }
    }
}

/* Menu Handler for setting up the algorithm runner */
static void launchAlgorithm(const char *caller) {
    Data *datafiles = NULL;

    if(strcmp(caller, "Exit") == 0) {
        ungetch('q');
        return;
    }

    datafiles = malloc(sizeof(Data) * 3);
    datafiles[0] = datafile("./data/dataset1.csv");
    datafiles[1] = datafile("./data/dataset2.csv");
    datafiles[2] = datafile("./data/dataset3.csv");

    teardown();

    if (strcmp(caller, "FCFS (First Come, First Serve)") == 0) {
         runAlgorithm(FCFS, datafiles, 3, 2);
    }

    if (strcmp(caller, "RR (Round Robin)") == 0) {
        runAlgorithm(RR, datafiles, 3, 2);
        runAlgorithm(RR, datafiles, 3, 4);
    }

    if (strcmp(caller, "SJF (Shortest Job First)") == 0) {
        runAlgorithm(SJF, datafiles, 3, 2);
    }

    free(datafiles);
    exit(0);
}

/* Wrapper for menu function */
Func menuFunction(void (*func)(const char *)) {
    Func menuFunc;
    menuFunc.fn = func;
    return menuFunc;
}

/* This guards against Control/Command-K;
   May cause minor graphical glitches */
static void rerender(void) {
    redrawwin(stdscr);
    refresh();
    redrawwin(appWindow);
}

/* Renders the top header, above the menu options */
static void renderHeader(WINDOW *win, int starty, int startx, int width, const char *header, chtype colour) {
    int length, x, y;

    if(win == NULL)
        win = stdscr;

    getyx(win, y, x);

    if(startx != 0)
        x = startx;
    if(starty != 0)
        y = starty;
    if(width == 0)
        width = 80;

    length = (int) strlen(header);
    x = startx + ((width - length)/ 2);
    wattron(win, colour);
    mvwprintw(win, y, x, "%s", header);
    wattroff(win, colour);

    mvwaddch(win, 2, 0, ACS_LTEE);
    mvwhline(win, 2, 1, ACS_HLINE, 38);
    mvwaddch(win, 2, 39, ACS_RTEE);
    refresh();
}

/* Renders the actual menu and sets up the main loop */
void render(const char **choices, size_t num) {
    const char * header = "Scheduler";
    int i, key;
    Func menuFunc = menuFunction(launchAlgorithm);

    numChoices = num;

    /* Init NCurses itself */
    init();

    menuItems = (ITEM **) calloc (numChoices + 1, sizeof (ITEM *));

    for(i = 0; i < (int) numChoices; i++) {
        menuItems[i] = new_item(choices[i], "");
        set_item_userptr(menuItems[i], &menuFunc);
    }

    menuItems[numChoices] = (ITEM *) NULL;
    appMenu = new_menu((ITEM **) menuItems);

    appWindow = newwin(10, 40, 0, 0);
    keypad(appWindow, TRUE);

    set_menu_win(appMenu, appWindow);
    set_menu_sub(appMenu, derwin(appWindow, 6, 38, 4, 1));

    set_menu_mark(appMenu, " > ");

    box(appWindow, 0, 0);
    renderHeader(appWindow, 1, 0, 40, header, COLOR_PAIR(1));

    set_menu_fore(appMenu, COLOR_PAIR(1));

    mvprintw(11, 0, " Press <ENTER> to select an Option");
    mvprintw(12, 0, " Up and Down Arrows to Navigate (q/Q to Exit, ^r to refresh)");
    refresh();

    post_menu(appMenu);
    wrefresh(appWindow);

    /* Main Loop */
    while((key = wgetch(appWindow)) != 'q' && key != 'Q') {
        switch (key) {
            case KEY_DOWN:
                menu_driver(appMenu, REQ_DOWN_ITEM);
                break;

            case KEY_UP:
                menu_driver(appMenu, REQ_UP_ITEM);
                break;

            case 10: { /* Enter */
                ITEM *cur = current_item(appMenu);
                const char *curName = item_name(cur);
                Func p = *(Func *) item_userptr(cur);

                p.fn(curName);
                break;
            }
            break;

            case 'r' & 0x1f:
                rerender();
                break;

            default:
                break;
        }
    }

    teardown();
}
