/* Peter Clay Holden Morris-Hind, 16062789 */

#include "util/save.h"

static const char *SAVE_DIR = "./save";

#ifndef TEST
static
#endif
void stripExtension(char *filepath) {
    char *end = filepath + strlen(filepath);

    while (end > filepath && *end != '.' && *end != '\\' && *end != '/') {
        --end;
    }

    if ((end > filepath && *end == '.') &&  (*(end - 1) != '\\' && *(end - 1) != '/')) {
        *end = '\0';
    }
}

static int createDir(void) {
    struct stat st = {0};

    if (stat(SAVE_DIR, &st) == -1) {
        return mkdir(SAVE_DIR, 0700);
    } else {
        return 1;
    }
}

static time_t getUtc(void) {
    time_t localTime;
    struct tm *ptm;

    time (&localTime);
    ptm = gmtime (&localTime);

    return mktime(ptm);
}

static int writeFile(SaveData data, char *fileName) {
    FILE *f = fopen(fileName, "w");
    int i;

    if (f == NULL) {
        fprintf(stderr, "scheduler: error creating %s\n", fileName);
        exit(1);
    }

    fprintf(f, "pid, at, bt\n");

    for(i = 0; i < data.data -> numItems; ++i) {
        fprintf(f, "%d, %d, %d\n",
            data.data -> process[i].procid,
            data.data -> process[i].arrival,
            data.data -> process[i].burst);
    }

    fprintf(f, "\nAv. Turnaround=%.3f, Av. Wait=%.3f\n",
        (double) data.averages -> turnaround, (double) data.averages -> wait);

    free(fileName);
    return 0;
}

int save (SaveData data, char *algName) {
    time_t timeStamp = getUtc();
    char *filename = malloc(sizeof(data.data -> filename));
    char *genFilename = malloc (sizeof(char) * 256);

    filename = p_strdup(data.data -> filename);
    stripExtension(filename);

    sprintf(genFilename, "./save/%s-%lx-%s.scdr", basename(filename), timeStamp, algName);

    if (createDir() == -1) {
        fprintf(stderr, "ERROR: Could not create directory %s", SAVE_DIR);
        exit(1);
    }

    printf("GENERATED FILENAME: %s\n\n", genFilename);

    free(filename);

    return writeFile(data, genFilename);
}
